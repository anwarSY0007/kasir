  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Penjualan</h1>
          <span style="color: red; font-size: 17px; pointer-events: none;">
            <?= validation_errors(); ?>
          </span>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
  <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-4">
                    <div class="small-box box-widget">
                        <div class="card-body">
                            <div class="box-body">
                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align:top">
                                            <label for="date">Date</label>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="date" id="date" value="<?= date('Y-m-d') ?>" class="form-control" readonly>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:top; width:30%">
                                            <label for="user">Kasir</label>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" id="user" value="#" class="form-control" readonly>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <label for="customer">Customer</label>
                                        </td>
                                        <td>
                                            <div>
                                                <select id="customer" class="form-control">
                                                    <option value="">Umum</option>
                                                    
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="small-box box-widget">
                        <div class="card-body">
                            <div class="box-body">
                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align:top; width:30%">
                                            <label for="barcode">Barcode</label>
                                        </td>
                                        <td>
                                            <div class="form-group input-group">
                                                <input type="hidden" id="item_id">
                                                <input type="hidden" id="harga_jual">
                                                <input type="hidden" id="stock">
                                                <input type="text" id="barcode" class="form-control" autofocus>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-item">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <label for="qty">Qty</label>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" id="qty" value="1" min="1" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div>
                                                <button type="button" id="add_cart" class="btn btn-primary">
                                                    <i class="fa fa-cart-plus"></i> tambah
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="small-box box-widget">
                        <div class="card-body">
                            <div class="box-body">
                                <div align="right">
                                    <h4>Invoice <b><span id="invoice"><?= $invoice ?></span></b></h4>
                                    <h1><b><span id="grand_total2" style="font-size:50pt"></span></b></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="row">
            <div class="col-lg-12">
                <div class="small-box box-widget">
                    <div class="card-body">
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Barcode</th>
                                        <th>Produk item</th>
                                        <th>Harga</th>
                                        <th>Qty</th>
                                        <th width="10%">Discount item</th>
                                        <th width="15%">Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="cart_table">
                                <?php $this->view('penjualan/data_penjualan') ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-4">
                    <div class="small-box box-widget">
                        <div class="card-body">
                            <div class="box-body">
                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align:top; width:30%">
                                            <label for="sub_total">Sub total</label>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" id="sub_total" value="" class="form-control" readonly>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <label for="discount">Discount</label>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" id="discount" value="" min="" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <label for="grand_total">Grand total</label>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" id="grand_total" class="form-control" readonly>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="small-box box-widget">
                        <div class="card-body">
                            <table width="100%">
                                <tr>
                                    <td style="vertical-align:top; width:30%">
                                        <label for="cash">Cash</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="number" id="cash" value="0" min="0" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:top">
                                        <label for="change">Kembalian</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="number" id="change" class="form-control" readonly>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3">
                    <div class="small-box box-widget">
                        <div class="card-body">
                            <table width="100%">
                                <tr>
                                    <td style="vertical-align:top">
                                        <label for="note">Note</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <textarea id="note" rows="3" class="form-control"></textarea>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="small-box box-widget">
            <div class="card-body">
                <button id="cancel_payment" class="btn btn-flat btn-warning">
                    <i class="fas fa-refresh"></i> Cancel
                </button>
                <button id="process_payment" class="btn btn-flat btn-success">
                    <i class="fa fa-paper-plane"></i> Proses Bayar
                </button>
            </div>
        </div>
    </div>
  </section>



  <!-- modal tambah produk item -->
<div class="modal fade" id="modal-item">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pilih produk item</h4>
            </div>
            <div class="modal-body" table-responsive>
                <table class="table table-bordered table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Barcode</th>
                            <th>Name</th>
                            <!-- <th>Unit</th> -->
                            <th>Harga</th>
                            <th>Stock</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($item as $i => $data) { ?>
                            <tr>
                                <td><?= $data['kode'] ?></td>
                                <td><?= $data['nama'] ?></td>
                                <!-- <td><?= $data->unit_name ?></td> -->
                                <td class="text-right"><?= indo_currency($data['harga_jual']) ?></td>
                                <td class="text-right"><?= $data['stok'] ?></td>
                                <td class="text-right">
                                    <button class="btn btn-xs btn-info" id="select" data-id="<?= $data['id'] ?>" data-barcode="<?= $data['kode'] ?>" data-harga="<?= $data['harga_jual'] ?>" data-stock="<?= $data['stok'] ?>">
                                        <i class="fa fa-check"></i> Select
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal edit produk item -->
<div class="modal fade" id="modal-item-edit">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Update produk item</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="cartid_item">
                <div class="form-group">
                    <label for="produk_item">Produk item</label>
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" id="barcode_item" class="form-control" readonly>
                        </div>
                        <div class="col-md-7">
                            <input type="text" id="produk_item" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="harga_item">Harga</label>
                    <input type="number" id="harga_item" min="0" class="form-control">
                </div>
                <div class="form-group">
                    <label for="qty_item">Qty</label>
                    <input type="number" id="qty_item" min="1" class="form-control">
                </div>
                <div class="form-group">
                    <label for="total_before">Total sebelum discount</label>
                    <input type="number" id="total_before" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label for="discount_item">Discount per item</label>
                    <input type="number" id="discount_item" min="0" class="form-control">
                </div>
                <div class="form-group">
                    <label for="total_item">Total setelah discount</label>
                    <input type="number" id="total_item" class="form-control" readonly>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" id="edit_cart" class="btn btn-flat btn-success">
                        <i class="fa fa-paper-plane"></i>Save
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
// ambil barang dari modal
   $(document).on('click', '#select', function() {
        $('#item_id').val($(this).data('id'))
        $('#barcode').val($(this).data('kode'))
        $('#harga_jual').val($(this).data('harga_jual'))
        $('#stock').val($(this).data('stock'))
        $('#modal-item').modal('hide')
    })
// masukan brang ke cart
    // $(document).on('click', '#add_cart', function() {
    //     var item_id = $('#item_id').val()
    //     var harga = $('#harga_jual').val()
    //     var stock = $('#stock').val()
    //     var qty = $('#qty').val()
    //     if (item_id == '') {
    //         alert('produk belum dipilih')
    //         $('#barcode').focus()
    //     } else if (stock < 1) {
    //         alert('stok tidak mencukupi')
    //         $('#item_id').val('')
    //         $('#barcode').val('')
    //         $('#barcode').focus()
    //     } else {
    //         $.ajax({
    //             type: 'post',
    //             url: '<?= site_url('sale/process_jual') ?>',
    //             data: {
    //                 'add_cart': true,
    //                 'item_id': item_id,
    //                 'harga': harga,
    //                 'qty': qty
    //             },
    //             dataType: 'json',
    //             success: function(result) {
    //                 if (result.success == true) {
    //                     $('#cart_table').load('<?= site_url('sale/cart_data') ?>', function() {
    //                         calculate()
    //                     })
    //                     $('#item_id').val('')
    //                     $('#barcode').val('')
    //                     $('#qty').val(1)
    //                     $('#barcode').focus()
    //                 } else {
    //                     alert('gagal tambah item cart')
    //                 }
    //             }
    //         })
    //     }
    // })
</script>