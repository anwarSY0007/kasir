<?php $no = 1;
if($cart->num_rows() > 0) {
    foreach ($cart->result() as $c => $data) { ?>
        <tr>
            <td><?=$no++?></td>
            <td><?=$data['kode']?></td>
            <td><?=$data['nama']?></td>
            <td class="text-right"><?=indo_currency($data->cart_harga)?></td>
            <td class="text-center"><?=$data->qty?></td>
            <td class="text-right"><?=$data->discount_item?></td>
            <td class="text-right" id="total"><?=$data->total?></td>
            <td class="text-center" width="160px">
                <button id="update_cart" data-toggle="modal" data-target="#modal-item-edit"
                data-cartid="<?=$data->cart_id?>"
                data-barcode="<?=$data->barcode?>"
                data-produk="<?=$data->item_name?>"
                data-harga="<?=$data->cart_harga?>"
                data-qty="<?=$data->qty?>"
                data-discount="<?=$data->discount_item?>"
                data-total="<?=$data->total?>"
                class="btn btn-xs btn-primary">
                    <i class="fa fa-pencil"></i> Ubah
                </button>
                <button id="del_cart" data-cartid="<?=$data->cart_id?>" class="btn btn-xs btn-danger">
                    <i class="fa fa-trash"></i> Hapus
                </button>
            </td>
        </tr>
        <?php
    }
} else {
    echo '<tr>
        <td colspan="8" class="text-center"> Barang belum dipilih</td>
    </tr>';
} ?>