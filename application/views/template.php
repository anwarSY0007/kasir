<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title id="titlepsc"> <?= $title ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= base_url('template/adminlte/dist/img/logobms.png'); ?>" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/jqvmap/jqvmap.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/toastr/toastr.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url('template/adminlte/') ?>plugins/summernote/summernote-bs4.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style type="text/css">
        body {
            font-size: 12px;
        }

        .modal-body {
            font-size: 14px;
        }

        .form-control {
            font-size: 14px;
        }

        .text-menu {
            color: white;
        }

        .nav-icon {
            color: white;
        }

        .fa-circle {
            color: yellow;
        }

        .fa-dot-circle {
            color: yellow;
        }

        .blink {
            animation: blink-animation 1s steps(5, start) infinite;
            -webkit-animation: blink-animation 1s steps(5, start) infinite;
        }

        @keyframes blink-animation {
            to {
                visibility: hidden;
            }
        }

        @-webkit-keyframes blink-animation {
            to {
                visibility: hidden;
            }
        }
    </style>
    <script src="<?= base_url('assets/js/waktu.js') ?>"></script>
</head>

<body class="hold-transition sidebar-mini layout-fixed <?= $this->uri->segment(1) == 'penjualan' ? 'sidebar-collapse' : null ?>">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;">
                        <img src="<?= base_url('template/adminlte/') ?>dist/img/user_icon.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $this->session->userdata('nama') ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header" style="cursor:pointer;">
                            <img src="<?= base_url('template/adminlte/') ?>dist/img/user_icon.png" class="img-circle" alt="User Image">
                            <p class="text-menu">
                                <?= $this->session->userdata('nama') ?>
                                <small></small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <a class="btn messages-menu bg-blue float-left" href="#">
                                Ubah Password
                            </a>
                            <a class="btn messages-menu bg-red float-right" href="<?= base_url('logout'); ?>">
                                <i class="fa fa-sign-out"></i>
                                Keluar
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: blue">
            <!-- Brand Logo -->
            <a href="<?= base_url() ?>" class="brand-link">
                <img src="<?= base_url('template/adminlte/') ?>dist/img/logobms.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light" style="font-size: 15px; color: white; font-weight: bold;">SEDOT TINJA ONLINE BMS</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <!-- <div style="text-align: center; padding-top: 10px;">
                    <a href="">
                        <img src="<?= base_url() ?>/template/adminlte/dist/img/avatar5.png" class="img-circle m-b" alt="logo" width="76">
                    </a>

                    <div style="color: white; margin-top: 15px;">
                        <span>
                            Nama :
                            <br>
                        </span>
                        <br>
                        <br>
                        <span>
                            Level :
                            <br>
                        </span>
                    </div>
                </div> -->

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- <li class="nav-item <?= $this->uri->segment(1) == 'home' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>">
                            <a href="<?= site_url() ?>" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p class="text-menu">
                                    HOME
                                </p>
                            </a>
                        </li>

                        <li class="nav-header">DATA MASTER</li>
                        <li class="nav-item">
                            <a href="<?= base_url('barang') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text-menu">
                                    Barang
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('kategori') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text-menu">
                                    Kategori Barang
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('supplier') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text-menu">
                                    Supplier
                                </p>
                            </a>
                        </li>

                        <li class="nav-header">TRANSAKSI</li>
                        <li class="nav-item">
                            <a href="<?= base_url('penjualan') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text-menu">
                                    Penjualan
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('pembelian') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text-menu">
                                    Pembelian
                                </p>
                            </a>
                        </li> -->
                        <li class="nav-header">HALAMAN AWAL</li>
                        <li class="nav-item <?= $this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>">
                            <a href="<?= site_url() ?>" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                Home
                                </p>
                            </a>
                            </li>
                            <li class="nav-header">DATA MASTER</li>
                            <li class="nav-item <?= $this->uri->segment(1) == 'supplier' ? 'class="active"' : '' ?>">
                            <a href="<?= site_url('supplier') ?>" class="nav-link">
                                <i class="nav-icon fa fa-truck"></i>
                                <p>
                                Supplier
                                </p>
                            </a>
                            </li>
                            <li class="nav-item <?= $this->uri->segment(1) == 'customer' ? 'class="active"' : '' ?>">
                            <a href="<?= site_url('customer') ?>" class="nav-link">
                                <i class="nav-icon fa fa-user"></i>
                                <p>Customer</p>
                            </a>
                            </li>
                            <li class="nav-header">DATA BARANG</li>
                            <li class="nav-item has-treeview <?= $this->uri->segment(1) == 'kategori' || $this->uri->segment(1) == 'satuan' || $this->uri->segment(1) == 'barang' ? 'active' : '' ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-archive"></i>
                                <p>
                                Produk
                                <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item <?= $this->uri->segment(1) == 'kategori' ? 'class="active"' : '' ?>">
                                <a href="<?= site_url('kategori') ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kategori</p>
                                </a>
                                </li>
                                <li class="nav-item <?= $this->uri->segment(1) == 'satuan' ? 'class="active"' : '' ?>">
                                <a href="<?= site_url('satuan') ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>satuan</p>
                                </a>
                                </li>
                                <li class="nav-item <?= $this->uri->segment(1) == 'barang' ? 'class="active"' : '' ?>">
                                <a href="<?= site_url('barang') ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Barang</p>
                                </a>
                                </li>
                            </ul>
                            </li>
                            <li class="nav-header">DATA TRANSAKSI</li>
                            <li class="nav-item has-treeview <?= $this->uri->segment(1) == 'Pembelian'|| $this->uri->segment(1) == 'penjualan' ? 'active' : '' ?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-shopping-cart"></i>
                                <p>
                                Transaksi
                                <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item <?= $this->uri->segment(1) == 'penjualan' ? 'class="active"' : '' ?>">
                                <a href="<?= site_url('penjualan') ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penjualan</p>
                                </a>
                                </li>
                                <li class="nav-item <?= $this->uri->segment(1) == 'pembelian' ? 'class="active"' : '' ?>">
                                <a href="<?= site_url('pembelian') ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pembelian</p>
                                </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper">

      <?php echo $contents ?>
      <!-- Main content -->
      <!-- /.content -->
    </div>
        <footer class="main-footer">
            <strong>Copyright &copy; <?= date('Y') ?> Bengkel Kita</strong> All rights
            reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.0.0-beta test
            </div>
        </footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script src="https://use.fontawesome.com/f0ff4c07f8.js"></script>

<!-- jQuery -->
<script src="<?= base_url('template/adminlte/') ?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('template/adminlte/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?= base_url('template/adminlte/') ?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('template/adminlte/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url('template/adminlte/') ?>plugins/select2/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/jszip/jszip.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- ChartJS -->
<script src="<?= base_url('template/adminlte/') ?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url('template/adminlte/') ?>plugins/sparklines/sparkline.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url('template/adminlte/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url('template/adminlte/') ?>plugins/moment/moment.min.js"></script>
<script src="<?= base_url('template/adminlte/') ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url('template/adminlte/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?= base_url('template/adminlte/') ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url('template/adminlte/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?= base_url('template/adminlte/') ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?= base_url('template/adminlte/') ?>plugins/toastr/toastr.min.js"></script>
<!-- bs-custom-file-input -->
<script src="<?= base_url('template/adminlte/') ?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?= base_url('template/adminlte/') ?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('template/adminlte/') ?>dist/js/adminlte.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url('template/adminlte/') ?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('template/adminlte/') ?>dist/js/demo.js"></script>

<!-- page script -->
<script type="text/javascript">
    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        $('#tgl_lahir').dateFormat({
            dateFormat: 'yy-mm-dd'
        }).val();
    });
</script>

</body>

</html>