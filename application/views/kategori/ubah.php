    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ubah Kategori</h1>
                    <!-- <span style="color: red; font-size: 17px; pointer-events: none;">
                        <?= validation_errors(); ?>
                    </span> -->
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Ubah Data</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="<?= base_url('kategori/ubah_proses') ?>" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group" hidden>
                                    <label for="exampleInputEmail1">ID<span class="text-red">*</span></label>
                                    <input type="text" class="form-control" name="id" id="id" value="<?= $kategori['id'] ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('id'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama<span class="text-red">*</span></label>
                                    <input type="text" class="form-control" name="nama" id="nama" value="<?= $kategori['nama']; ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('nama'); ?>
                                    </span>
                                </div>
                                <div class="footer">
                                    <a href="<?= base_url('kategori') ?>" class="btn btn-default">Kembali</a>
                                    <button type="submit" name="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>