<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>satuan</h1>
          <span style="color: red; font-size: 17px; pointer-events: none;">
            <?= validation_errors(); ?>
          </span>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data</h3>
        </div>
        <div class="card-body">
          <div class="mb-2">
            <a href="<?= base_url('satuan/tambah') ?>" class="btn btn-danger btn-sm shadow"><i class="fa fa-plus"> Tambah Data </i></a>
          </div>
          <table id="table_data" class="table table-bordered table-striped">
            <thead>
              <tr style="text-align: center;">
                <th>No</th>
                <th>Nama</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($data as $row) {
              ?>
                <tr style="text-align: center">
                  <td style="width: 3px;">
                    <?= $no++ ?>
                  </td>
                  <td>
                    <?= $row['nama_satuan'] ?>
                  </td>
                  <td style="width: 20%;">
                    <a href="<?= base_url('satuan/ubah/' . $row['id']) ?>" class="btn btn-primary btn-xs shadow">Ubah</a>
                    <button onclick="modal_hapus(<?= $row['id'] ?>)" class="btn btn-outline-danger btn-xs" data-toggle="modal" data-target="#modal-hapus">Hapus</button>
                  </td>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  </section>

<div class="modal fade" id="modal-hapus">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Hapus Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?= base_url('satuan/hapus') ?>" enctype="multipart/form-data" class="form-horizontal">
        <div class="modal-body">
          <div class="card-body">
            <p style="text-align: center; font-weight: bold;">Apakah yakin ingin menghapus data?</p>
            <input type="text" name="id" id="id_hapus" class="form-control" required readonly hidden>
            <div class="form-group row">
              <div class="col-md-6" style="text-align: center;">
                <button class="btn btn-success col-md-8" data-toggle="modal" type="submit">Ya</button>
              </div>
              <div class="col-md-6" style="text-align: center;">
                <button class="btn btn-danger col-md-8" data-toggle="modal" data-dismiss="modal">Tidak</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
  $(function() {
    $("#table_data").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#table_data_wrapper .col-md-6:eq(0)');
  });

  function modal_hapus(id) {
    $('#id_hapus').val(id);
  }
</script>