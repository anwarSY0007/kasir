<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Barang</h1>
                    <!-- <span style="color: red; font-size: 17px; pointer-events: none;">
                        <?= validation_errors(); ?>
                    </span> -->
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="<?= base_url('barang/tambah_proses') ?>" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama<span class="text-red">*</span></label>
                                    <input type="text" class="form-control" name="nama" id="nama" value="<?= set_value('nama'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('nama'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Kategori<span class="text-red">*</span></label>
                                    <select class="form-control select2" style="width: 100%;" name="id_kategori" id="id_kategori" required>
                                        <option value="">Pilih kategori</option>
                                        <?php
                                        foreach ($kategori as $row) {
                                        ?>
                                            <option value="<?= $row['id'] ?>" <?= set_select('id_territory', $row['id']); ?>><?= $row['nama'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('id_territory'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Stok<span class="text-red">*</span></label>
                                    <input type="text" class="form-control" name="stok" id="stok" value="<?= set_value('stok'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('stok'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga jual<span class="text-red">*</span></label>
                                    <input type="number" class="form-control" name="harga_jual" id="harga_jual" value="<?= set_value('harga_jual'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('harga_jual'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga beli<span class="text-red">*</span></label>
                                    <input type="number" class="form-control" name="harga_beli" id="harga_beli" value="<?= set_value('harga_beli'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('harga_beli'); ?>
                                    </span>
                                </div>
                                <div class="footer">
                                    <a href="<?= base_url('harga') ?>" class="btn btn-default">Kembali</a>
                                    <button type="submit" name="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>