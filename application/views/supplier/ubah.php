<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ubah Kategori</h1>
                    <!-- <span style="color: red; font-size: 17px; pointer-events: none;">
                        <?= validation_errors(); ?>
                    </span> -->
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Ubah Data</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="<?= base_url('supplier/ubah_proses') ?>" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="id" id="id" value="<?= $supplier['id'] ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('id'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama</label><span class="text-red">*</span>
                                    <input type="text" class="form-control" name="nama" id="nama" value="<?= $supplier['nama']; ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('nama'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No Telp</label><span class="text-red">*</span>
                                    <input type="text" class="form-control" name="no_telp" id="no_telp" value="<?= $supplier['no_telp']; ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('no_telp'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Provinsi</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="prov_id" id="prov_id" required>
                                        <option value="">Provinsi</option>
                                        <?php
                                        foreach ($provinsi as $row) {
                                        ?>
                                            <option value="<?= $row['id'] ?>" <?= $supplier['prov_id'] == $row['id'] ? 'selected' : '' ?>><?= $row['provinsi'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('prov_id'); ?>
                                    </span>
                                    <input type="hidden" name="prov_nama" id="prov_nama" value="<?= $supplier['prov_nama'] ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kabupaten</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="kab_id" id="kab_id" required>
                                        <option value="">Kabupaten</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('kab_id'); ?>
                                    </span>
                                    <input type="hidden" name="kab_nama" id="kab_nama" value="<?= $supplier['kab_nama'] ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kecamatan</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="kec_id" id="kec_id" required>
                                        <option value="">Kecamatan</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('kec_id'); ?>
                                    </span>
                                    <input type="hidden" name="kec_nama" id="kec_nama" value="<?= $supplier['kec_nama'] ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kelurahan</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="kel_id" id="kel_id" required>
                                        <option value="">Kelurahan</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('kel_id'); ?>
                                    </span>
                                    <input type="hidden" name="kel_nama" id="kel_nama" value="<?= $supplier['kel_nama'] ?>">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label><span class="text-red">*</span>
                                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?= $supplier['alamat']; ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('alamat'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" class="form-control" name="rt" id="rt" value="<?= $supplier['rt']; ?>">
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('rt'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" class="form-control" name="rw" id="rw" value="<?= $supplier['rw']; ?>">
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('rw'); ?>
                                    </span>
                                </div>
                                <div class="footer">
                                    <a href="<?= base_url('supplier') ?>" class="btn btn-default">Kembali</a>
                                    <button type="submit" name="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



<script>
    $.ajax({
        type: "GET",
        url: '<?= base_url("Welcome/kabByID/") ?>' + <?= $supplier['kab_id'] ?>,
        dataType: 'json',
        success: function(response) {
            if (response["status"] == 1) {
                let val = response["data"]
                kab = "<option value='" + val["id"] + "'>" + val["kabupaten"] + "</option>";
            } else {
                kab = "<option value=''>Pilih Provinsi Terlebih Dahulu</option>";
            }
            $('#kab_id').html(kab);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // alert(xhr.responseText)
        }
    })

    $.ajax({
        type: "GET",
        url: '<?= base_url("Welcome/kecByID/") ?>' + <?= $supplier['kec_id'] ?>,
        dataType: 'json',
        success: function(response) {
            if (response["status"] == 1) {
                let val = response["data"]
                kec = "<option value='" + val["id"] + "'>" + val["kecamatan"] + "</option>";
            } else {
                kec = "<option value=''>Pilih Kabupaten Terlebih Dahulu</option>";
            }
            $('#kec_id').html(kec);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // alert(xhr.responseText)
        }
    })

    $.ajax({
        type: "GET",
        url: '<?= base_url("Welcome/kelByID/") ?>' + <?= $supplier['kel_id'] ?>,
        dataType: 'json',
        success: function(response) {
            if (response["status"] == 1) {
                let val = response["data"]
                kel = "<option value='" + val["id"] + "'>" + val["kel"] + "</option>";
            } else {
                kel = "<option value=''>Pilih Kecamatan Terlebih Dahulu</option>";
            }
            $('#kel_id').html(kel);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // alert(xhr.responseText)
        }
    })



    $("#prov_id").change(function(e) {
        $("#kab_nama").val('');
        $("#kec_nama").val('');
        $("#kel_nama").val('');

        $("#kab_id").html("<option value=''>Pilih Provinsi Terlebih Dahulu</option>")
        $("#kec_id").html("<option value=''>Pilih Kabupaten Terlebih Dahulu</option>")
        $("#kel_id").html("<option value=''>Pilih Kecamatan Terlebih Dahulu</option>")

        let id_prov = $("#prov_id").find(":selected").val()
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/kab/") ?>' + id_prov,
            dataType: 'json',
            success: function(response) {
                let kab = "<option value=''>Pilih Kabupaten</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        kab += "<option value='" + val["id"] + "'>" + val["kabupaten"] + "</option>";
                    })
                }
                $('#kab_id').html(kab);
                $("#prov_nama").val($("#prov_id").find(":selected").text());
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })

    $("#kab_id").change(function(e) {
        $("#kec_id").html("<option value=''>Pilih Kabupaten Terlebih Dahulu</option>")
        $("#kel_id").html("<option value=''>Pilih Kecamatan Terlebih Dahulu</option>")
        let id_kab = $("#kab_id").find(":selected").val()
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/kec/") ?>' + id_kab,
            dataType: 'json',
            success: function(response) {
                let kec = "<option value=''>Pilih Kecamatan</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        kec += "<option value='" + val["id"] + "'>" + val["kecamatan"] + "</option>";
                    })
                }
                $('#kec_id').html(kec);
                $("#kab_nama").val($("#kab_id").find(":selected").text());
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })

    $("#kec_id").change(function(e) {
        $("#kel_id").html("<option value=''>Pilih Kecamatan Terlebih Dahulu</option>")
        let id_kec = $("#kec_id").find(":selected").val()
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/kel/") ?>' + id_kec,
            dataType: 'json',
            success: function(response) {
                let kel = "<option value=''>Pilih Desa / Kelurahan</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        kel += "<option value='" + val["id"] + "'>" + val["kel"] + "</option>";
                    })
                }
                $('#kel_id').html(kel);
                $("#kec_nama").val($("#kec_id").find(":selected").text());
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })

    $("#kel_id").change(function(e) {
        $("#kel_nama").val($("#kel_id").find(":selected").text());
    })


    // function changePenjamin(e) {
    //     let ref = $(e).find(":selected").val()
    //     if (ref == 1 || ref == '') {
    //         $("#div_kartu_penjamin").hide()
    //     } else {
    //         $("#div_kartu_penjamin").show()
    //     }
    // }
</script>