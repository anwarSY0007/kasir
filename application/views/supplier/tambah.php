<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Supplier</h1>
                    <!-- <span style="color: red; font-size: 17px; pointer-events: none;">
                        <?= validation_errors(); ?>
                    </span> -->
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="<?= base_url('supplier/tambah_proses') ?>" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama</label><span class="text-red">*</span>
                                    <input type="text" class="form-control" name="nama" id="nama" value="<?= set_value('nama'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('nama'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No Telp</label><span class="text-red">*</span>
                                    <input type="text" class="form-control" name="no_telp" id="no_telp" value="<?= set_value('no_telp'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('no_telp'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Provinsi</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="prov_id" id="prov_id" required>
                                        <option value="">Provinsi</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('prov_id'); ?>
                                    </span>
                                    <input type="hidden" name="prov_nama" id="prov_nama">
                                </div>
                                <div class="form-group">
                                    <label>Kabupaten</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="kab_id" id="kab_id" required>
                                        <option value="">Kabupaten</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('kab_id'); ?>
                                    </span>
                                    <input type="hidden" name="kab_nama" id="kab_nama">
                                </div>
                                <div class="form-group">
                                    <label>Kecamatan</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="kec_id" id="kec_id" required>
                                        <option value="">Kecamatan</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('kec_id'); ?>
                                    </span>
                                    <input type="hidden" name="kec_nama" id="kec_nama">
                                </div>
                                <div class="form-group">
                                    <label>Kelurahan</label><span class="text-red">*</span>
                                    <select class="form-control" style="width: 100%;" name="kel_id" id="kel_id" required>
                                        <option value="">Kelurahan</option>
                                    </select>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('kel_id'); ?>
                                    </span>
                                    <input type="hidden" name="kel_nama" id="kel_nama">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label><span class="text-red">*</span>
                                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?= set_value('alamat'); ?>" required>
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('alamat'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" class="form-control" name="rt" id="rt" value="<?= set_value('rt'); ?>">
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('rt'); ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" class="form-control" name="rw" id="rw" value="<?= set_value('rw'); ?>">
                                    <span style="color: red; font-size: 15px; pointer-events: none;">
                                        <?= form_error('rw'); ?>
                                    </span>
                                </div>
                                <div class="footer">
                                    <a href="<?= base_url('supplier') ?>" class="btn btn-default">Kembali</a>
                                    <button type="submit" name="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $("#prov_id").change(function(e) {

        $("#kab_id").html("<option value=''>Pilih Provinsi Terlebih Dahulu</option>")
        $("#kec_id").html("<option value=''>Pilih Kabupaten Terlebih Dahulu</option>")
        $("#kel_id").html("<option value=''>Pilih Kecamatan Terlebih Dahulu</option>")

        let id_prov = $("#prov_id").find(":selected").val()
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/kab/") ?>' + id_prov,
            dataType: 'json',
            success: function(response) {
                let kab = "<option value=''>Pilih Kabupaten</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        kab += "<option value='" + val["id"] + "'>" + val["kabupaten"] + "</option>";
                    })
                }
                $('#kab_id').html(kab);
                $("#prov_nama").val($("#prov_id").find(":selected").text());
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })

    $("#kab_id").change(function(e) {
        $("#kec_id").html("<option value=''>Pilih Kabupaten Terlebih Dahulu</option>")
        $("#kel_id").html("<option value=''>Pilih Kecamatan Terlebih Dahulu</option>")
        let id_kab = $("#kab_id").find(":selected").val()
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/kec/") ?>' + id_kab,
            dataType: 'json',
            success: function(response) {
                let kec = "<option value=''>Pilih Kecamatan</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        kec += "<option value='" + val["id"] + "'>" + val["kecamatan"] + "</option>";
                    })
                }
                $('#kec_id').html(kec);
                $("#kab_nama").val($("#kab_id").find(":selected").text());
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })

    $("#kec_id").change(function(e) {
        $("#kel_id").html("<option value=''>Pilih Kecamatan Terlebih Dahulu</option>")
        let id_kec = $("#kec_id").find(":selected").val()
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/kel/") ?>' + id_kec,
            dataType: 'json',
            success: function(response) {
                let kel = "<option value=''>Pilih Desa / Kelurahan</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        kel += "<option value='" + val["id"] + "'>" + val["kel"] + "</option>";
                    })
                }
                $('#kel_id').html(kel);
                $("#kec_nama").val($("#kec_id").find(":selected").text());
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })

    $("#kel_id").change(function(e) {
        $("#kel_nama").val($("#kel_id").find(":selected").text());
    })



    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: '<?= base_url("Welcome/prov/") ?>',
            dataType: 'json',
            success: function(response) {
                let hasil = "<option value=''>Pilih Provinsi</option>"
                if (response["status"] == 1) {
                    $.each(response["data"], function(index) {
                        let val = response["data"][index]
                        hasil += "<option value='" + val["id"] + "'>" + val["provinsi"] + "</option>";
                    })
                }
                $('#prov_id').html(hasil);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText)
            }
        })
    })
</script>