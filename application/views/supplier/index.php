<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Supplier</h1>
          <span style="color: red; font-size: 17px; pointer-events: none;">
            <?= validation_errors(); ?>
          </span>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data</h3>
        </div>
        <div class="card-body">
          <div class="mb-2">
            <a href="<?= base_url('supplier/tambah') ?>" class="btn btn-danger btn-sm shadow"><i class="fa fa-plus"> Tambah Data </i></a>
          </div>
          <table id="table_data" class="table table-bordered table-striped">
            <thead>
              <tr style="text-align: center;">
                <th>No</th>
                <th>Nama</th>
                <th>No Telp</th>
                <th>Alamat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($data as $row) {
              ?>
                <tr style="text-align: center">
                  <td style="width: 3px;">
                    <?= $no++ ?>
                  </td>
                  <td>
                    <?= $row['nama'] ?>
                  </td>
                  <td>
                    <?= $row['no_telp'] ?>
                  </td>
                  <td>
                    <?= $row['alamat']
                      . ' RT ' . $row['rt'] . '/' . $row['rw']
                      . ', ' . $row['kel_nama']
                      . ', ' . $row['kec_nama']
                      . ', ' . $row['kab_nama']
                      . ', ' . $row['prov_nama']
                    ?>
                  </td>
                  <td style="width: 20%;">
                    <a href="<?= base_url('supplier/ubah/' . $row['id']) ?>" class="btn btn-primary btn-xs shadow">Ubah</a>
                    <button onclick="modal_hapus(<?= $row['id'] ?>)" class="btn btn-outline-danger btn-xs" data-toggle="modal" data-target="#modal-hapus">Hapus</button>
                  </td>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- <div class="card card-default">
        <div class="card-header">
          <h3 class="card-title">Data</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Minimal</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
              <div class="form-group">
                <label>Disabled</label>
                <select class="form-control select2" disabled="disabled" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Multiple</label>
                <select class="select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                  <option>Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
              <div class="form-group">
                <label>Disabled Result</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option disabled="disabled">California (disabled)</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
            </div>
          </div>

          <h5>Custom Color Variants</h5>
          <div class="row">
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label>Minimal (.select2-danger)</label>
                <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
            </div>
            <div class="col-12 col-sm-6">
              <div class="form-group">
                <label>Multiple (.select2-purple)</label>
                <div class="select2-purple">
                  <select class="select2" multiple="multiple" data-placeholder="Select a State" data-dropdown-css-class="select2-purple" style="width: 100%;">
                    <option>Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </section>
</div>

<div class="modal fade" id="modal-hapus">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          Hapus Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?= base_url('supplier/hapus') ?>" enctype="multipart/form-data" class="form-horizontal">
        <div class="modal-body">
          <div class="card-body">
            <p style="text-align: center; font-weight: bold;">Apakah yakin ingin menghapus data?</p>
            <input type="text" name="id" id="id_hapus" class="form-control" required readonly hidden>
            <div class="form-group row">
              <div class="col-md-6" style="text-align: center;">
                <button class="btn btn-success col-md-8" data-toggle="modal" type="submit">Ya</button>
              </div>
              <div class="col-md-6" style="text-align: center;">
                <button class="btn btn-danger col-md-8" data-toggle="modal" data-dismiss="modal">Tidak</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
  $(function() {
    $("#table_data").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#table_data_wrapper .col-md-6:eq(0)');
  });

  function modal_hapus(id) {
    $('#id_hapus').val(id);
  }
</script>