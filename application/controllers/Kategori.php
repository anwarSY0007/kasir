<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();


        $this->load->model("Kategori_model");
    }

    public function data()
    {
        $data = $this->Kategori_model->get_all();

        if ($data) {
            echo json_encode([
                "status"    => 1,
                "message"   => "Data ditemukan",
                "data"      => $data
            ]);
        } else {
            echo json_encode([
                "status"    => 0,
                "message"   => "Data tidak ditemukan",
            ]);
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Kategori',
            'data' => $this->Kategori_model->get_all() ? $this->Kategori_model
                ->order_by('nama', 'asc')
                ->get_all() : []
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('kategori/index', $data);
        $this->load->view('includes/footer');
        // echo json_encode($data);
    }

    public function tambah()
    {
        $data = [
            'title' => 'Kategori',
            'kategori' => $this->Kategori_model->get_all()
        ];
        $this->template->load('template','kategori/tambah',$data);
        // $this->load->view('includes/header', $data);
        // $this->load->view('kategori/tambah', $data);
        // $this->load->view('includes/footer');
    }

    public function tambah_proses()
    {
        // $data = [
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'created_at'        => date('Y-m-d H:i:s'),
        // ];

        $data = [
            'nama'              => $this->input->post('nama', true),
        ];

        $config = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == false) {
            $this->tambah();
        } else {
            $this->Kategori_model->insert($data);
            redirect('kategori');
        }
    }

    public function ubah($id)
    {
        $data = [
            'title' => 'Kategori',
            'kategori' => $this->Kategori_model->get($id)
        ];
        // $this->load->view('includes/header', $data);
        $this->template->load('template','kategori/ubah', $data);
        // $this->load->view('includes/footer');
    }

    public function ubah_proses()
    {
        $data = [
            'id'    => $this->input->post('id'),
            'ubah'  => [
                'nama'  => $this->input->post('nama', true),
            ],
        ];

        $config = [
            [
                'field' => 'id',
                'label' => 'ID',
                'rules' => 'required|trim|numeric'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == false) {
            $this->ubah($data['id']);
        } else {
            if ($data['id'] == null) {
                $this->session->set_flashdata(
                    'message',
                    '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p style="font-weight: bold; font-size: 50px; text-align: center; color: white;">Terjadi Kesalahan!</p>
            </div>'
                );
                redirect('kategori');
            } else {
                $this->Kategori_model->update($data['ubah'], $data['id']);
                redirect('kategori');
            }
        }
    }

    public function hapus()
    {
        // $data = [
        //     'id'    => $this->input->post('id', true),
        //     'ubah'  => [
        //         'is_deleted'    => 1
        //     ],
        // ];

        $id = $this->input->post('id', true);

        if ($id == null) {
            $this->session->set_flashdata(
                'message',
                '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p style="font-weight: bold; font-size: 50px; text-align: center; color: white;">Terjadi Kesalahan!</p>
                </div>'
            );
            redirect('kategori');
        } else {
            $this->Kategori_model->delete($id);
            redirect('kategori');
        }
    }
}
