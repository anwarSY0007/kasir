<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();


        $this->load->model("Supplier_model");
    }

    public function data()
    {
        $data = $this->Supplier_model->get_all();

        if ($data) {
            echo json_encode([
                "status"    => 1,
                "message"   => "Data ditemukan",
                "data"      => $data
            ]);
        } else {
            echo json_encode([
                "status"    => 0,
                "message"   => "Data tidak ditemukan",
            ]);
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Supplier',
            'data' => $this->Supplier_model->get_all() ? $this->Supplier_model
                ->order_by('nama', 'asc')
                ->get_all() : []
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('supplier/index', $data);
        $this->load->view('includes/footer');
        // echo json_encode($data);
    }

    public function tambah()
    {
        $data = [
            'title' => 'Supplier',
            'supplier' => $this->Supplier_model->get_all()
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('supplier/tambah', $data);
        $this->load->view('includes/footer');
    }

    public function tambah_proses()
    {
        // $data = [
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'created_at'        => date('Y-m-d H:i:s'),
        // ];

        $data = [
            'nama'          => $this->input->post('nama', true),
            'no_telp'       => $this->input->post('no_telp', true),
            'prov_id'       => $this->input->post('prov_id', true),
            'prov_nama'     => $this->input->post('prov_nama', true),
            'kab_id'        => $this->input->post('kab_id', true),
            'kab_nama'      => $this->input->post('kab_nama', true),
            'kec_id'        => $this->input->post('kec_id', true),
            'kec_nama'      => $this->input->post('kec_nama', true),
            'kel_id'        => $this->input->post('kel_id', true),
            'kel_nama'      => $this->input->post('kel_nama', true),
            'alamat'        => $this->input->post('alamat', true),
            'rt'            => $this->input->post('rt', true),
            'rw'            => $this->input->post('rw', true),
        ];

        $config = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|trim|numeric|min_length[10]|max_length[13]'
            ],
            [
                'field' => 'prov_id',
                'label' => 'Provinsi',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'kab_id',
                'label' => 'Kabupaten',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'kec_id',
                'label' => 'Kecamatan',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'kel_id',
                'label' => 'Kelurahan',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == false) {
            $this->tambah();
        } else {
            $this->Supplier_model->insert($data);
            redirect('supplier');
        }
    }

    public function ubah($id)
    {
        $data = [
            'title' => 'Supplier',
            'supplier' => $this->Supplier_model->get($id),
            'provinsi' => $this->db->get('tm_provinsi')->result_array()
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('supplier/ubah', $data);
        $this->load->view('includes/footer');
    }

    public function ubah_proses()
    {
        $data = [
            'id'    => $this->input->post('id'),
            'ubah'  => [
                'nama'          => $this->input->post('nama', true),
                'no_telp'       => $this->input->post('no_telp', true),
                'prov_id'       => $this->input->post('prov_id', true),
                'prov_nama'     => $this->input->post('prov_nama', true),
                'kab_id'        => $this->input->post('kab_id', true),
                'kab_nama'      => $this->input->post('kab_nama', true),
                'kec_id'        => $this->input->post('kec_id', true),
                'kec_nama'      => $this->input->post('kec_nama', true),
                'kel_id'        => $this->input->post('kel_id', true),
                'kel_nama'      => $this->input->post('kel_nama', true),
                'alamat'        => $this->input->post('alamat', true),
                'rt'            => $this->input->post('rt', true),
                'rw'            => $this->input->post('rw', true),
            ],
        ];

        $config = [
            [
                'field' => 'id',
                'label' => 'ID',
                'rules' => 'required|trim|numeric'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|trim|numeric|min_length[10]|max_length[13]'
            ],
            [
                'field' => 'prov_id',
                'label' => 'Provinsi',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'kab_id',
                'label' => 'Kabupaten',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'kec_id',
                'label' => 'Kecamatan',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'kel_id',
                'label' => 'Kelurahan',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == false) {
            $this->ubah($data['id']);
        } else {
            if ($data['id'] == null) {
                $this->session->set_flashdata(
                    'message',
                    '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p style="font-weight: bold; font-size: 50px; text-align: center; color: white;">Terjadi Kesalahan!</p>
            </div>'
                );
                redirect('supplier');
            } else {
                $this->Supplier_model->update($data['ubah'], $data['id']);
                redirect('supplier');
            }
        }
    }

    public function hapus()
    {
        // $data = [
        //     'id'    => $this->input->post('id', true),
        //     'ubah'  => [
        //         'is_deleted'    => 1
        //     ],
        // ];

        $id = $this->input->post('id', true);

        if ($id == null) {
            $this->session->set_flashdata(
                'message',
                '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p style="font-weight: bold; font-size: 50px; text-align: center; color: white;">Terjadi Kesalahan!</p>
                </div>'
            );
            redirect('supplier');
        } else {
            $this->Supplier_model->delete($id);
            redirect('supplier');
        }
    }
}
