<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Admin_model');
		$this->tgl = date('Y-m-d');
	}

	public function index()
	{
		if ($this->session->userdata('username') == "") {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Username', 'trim|required|min_length[6]');
			// $this->form_validation->set_rules('level', 'LEVEL', 'trim|required');
			if ($this->form_validation->run() == false) {
				$this->load->view('login');
			} else {
				$this->_loginproses();
			}
		} else {
			$this->hal_awal();
		}
	}

	public function hal_awal()
	{
		$data = [
			'title' => 'Kasir Bengkel',
			'box_title' => 'KASIR LUR'
		];
		$this->load->view('includes/header', $data);
		$this->load->view('index', $data);
		$this->load->view('includes/footer');
	}

	private function _loginproses()
	{

		$val_log = [
			'username' => $this->input->post('username', true),
			'password' => md5($this->input->post('password', true)),
		];

		$login = $this->Admin_model
			->where([
				'username' => $val_log['username'],
				'password' => $val_log['password'],
			])
			->with_level('fields:nama')
			->get();



		$data_login = [
			'id'            => $login['id'],
			'username'      => $login['username'],
			'nama'          => $login['nama'],
			'level'         => $login['level']['nama'],
		];

		// $save_login = [
		// 	'username' => $val_log['username'],
		// 	'id_level' => $val_log['level'],
		// 	'created_at' => date('Y-m-d H:i:s')
		// ];

		if ($login == NULL) {
			$this->session->set_flashdata(
				'message_login',
				'<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>            
				<p style="font-weight: bold; font-size: 30px; text-align: center; color: black;">USERNAME / PASSWORD SALAH !!!</p>
				</div>'
			);
			// $save_login['keterangan'] = 'USERNAME / PASSWORD SALAH';
			// $this->db->insert('log_login', $save_login);
			redirect(base_url(''));
		} else {
			if ($login['deleted_at'] != null) {
				$this->session->set_flashdata(
					'message_login',
					'<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>            
						<p style="font-weight: bold; font-size: 30px; text-align: center; color: black;">User tidak aktif. <br> Silahkan Hubungi Admin!</p>
						</div>'
				);
				// $save_login['keterangan'] = 'USER TIDAK AKTIF';
				// $this->db->insert('log_login', $save_login);
				redirect(base_url(''));
			} else {
				// $this->User_Model->save_login($save_login);
				// $this->User_Model->update_lastlogin($nik);
				// $save_login['keterangan'] = 'BERHASIL LOGIN';
				// $this->db->insert('log_login', $save_login);
				// if ($login['level'] == 'poli') {
				// 	$data_login['ruang_poli'] = $dokter['ruang_poli'];
				// }
				$this->session->set_userdata($data_login);
				redirect(base_url());
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(''));
	}

	public function prov()
	{
		$prov = $this->db
			->order_by('provinsi', 'asc')
			->get("tm_provinsi")->result_array();

		if ($prov) {
			echo json_encode([
				"status"    => 1,
				"data"      => $prov
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}

	public function kab($id_prov = "")
	{
		$prov = $this->db
			->order_by('kabupaten', 'asc')
			->where(["id_provinsi" => $id_prov])
			->get("tm_kabupaten")
			->result_array();

		if ($prov) {
			echo json_encode([
				"status"    => 1,
				"data"      => $prov
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}

	public function kec($id_kab = "")
	{
		$prov = $this->db
			->order_by('kecamatan', 'asc')
			->where(["id_kabupaten" => $id_kab])
			->get("tm_kecamatan")
			->result_array();

		if ($prov) {
			echo json_encode([
				"status"    => 1,
				"data"      => $prov
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}

	public function kel($id_kec = "")
	{
		$prov = $this->db
			->query("SELECT * FROM tm_kelurahan WHERE id LIKE '$id_kec%' order by kel asc")
			->result_array();

		if ($prov) {
			echo json_encode([
				"status"    => 1,
				"data"      => $prov
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}

	public function kabByID($id = null)
	{
		$this->db->where('id', $id);
		$kab = $this->db->get('tm_kabupaten')->row_array();

		if ($kab) {
			echo json_encode([
				"status"    => 1,
				"data"      => $kab
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}

	public function kecByID($id = null)
	{
		$this->db->where('id', $id);
		$kec = $this->db->get('tm_kecamatan')->row_array();

		if ($kec) {
			echo json_encode([
				"status"    => 1,
				"data"      => $kec
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}

	public function kelByID($id = null)
	{
		$this->db->where('id', $id);
		$kel = $this->db->get('tm_kelurahan')->row_array();

		if ($kel) {
			echo json_encode([
				"status"    => 1,
				"data"      => $kel
			]);
		} else {
			echo json_encode([
				"status"    => 0,
				"data"      => []
			]);
		}
	}
}
