<?php

class Penjualan extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('penjualan_model');
	}
	
    public function index()
	{
		$this->load->model('barang_model');
		$item = $this->barang_model
		->with_kategoris('fields:nama')
		->get_all();

		$cart = $this->penjualan_model->get_keranjang();
		$data = [
			'title' => 'Form Penjualan',
			'box_title' => 'Form Penjualan',
			'invoice' => $this->penjualan_model->invoice_no(),
			'item'=>$item,
			'cart'=>$cart,

		];
		$this->template->load('template','penjualan/index',$data);
	}
	public function process_jual()
    {
		$data = $this->input->post(null, TRUE);

		if(isset($_POST['add_cart'])) {

			$item_id = $this->input->post('item_id');
			$check_cart = $this->penjualan_model->get_cart(['tcart.item_id' => $item_id])->num_rows();
			if($check_cart > 0) {
				$this->penjualan_model->update_cart_qty($data);
			} else {
				$this->penjualan_model->add_cart($data);
			}

			if($this->db->affected_rows() > 0) {
				$params = array("success" => true);
			} else {
				$params = array("success" => false);
			}
			echo json_encode($params);		
		}

		if(isset($_POST['edit_cart'])) {
			$this->penjualan_model->edit_cart($data);

			if($this->db->affected_rows() > 0) {
				$params = array("success" => true);
			} else {
				$params = array("success" => false);
			}
			echo json_encode($params);
		}

		if(isset($_POST['process_payment'])) {
			$sale_id = $this->penjualan_model->add_sale($data);
			$cart = $this->penjualan_model->get_cart()->result();
			$row = [];
			foreach($cart as $c => $value) {
				array_push($row, array(
					'sale_id' => $sale_id,
					'item_id' => $value->item_id,
					'harga' => $value->harga,
					'qty' => $value->qty,
					'discount_item' => $value->discount_item,
					'total' => $value->total,
					)
				);
			} 
			$this->penjualan_model->add_sale_detail($row);
			$this->penjualan_model->del_cart(['user_id' => $this->session->userdata('userid')]);

			if($this->db->affected_rows() > 0) {
				$params = array("success" => true, "sale_id" => $sale_id);
			} else {
				$params = array("success" => false);
			}
			echo json_encode($params);
		}
	}
}