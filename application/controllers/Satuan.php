<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('satuan_model');
        
    }
    public function data()
    {
        $data = $this->satuan_model->get_all();

        if ($data) {
            echo json_encode([
                "status"    => 1,
                "message"   => "Data ditemukan",
                "data"      => $data
            ]);
        } else {
            echo json_encode([
                "status"    => 0,
                "message"   => "Data tidak ditemukan",
            ]);
        }
    }
    
    public function index()
    {
        $data = [
            'title' => 'Satuan',
            'data' => $this->satuan_model->get_all() ? $this->satuan_model
                ->order_by('nama', 'asc')
                ->get_all() : []
        ];
        $this->template->load('template','satuan/index',$data);
    }
    public function tambah()
    {
        $data = [
            'title' => 'satuan',
            'satuan' => $this->satuan_model->get_all()
        ];
        $this->template->load('template','satuan/tambah',$data);
        // $this->load->view('includes/header', $data);
        // $this->load->view('satuan/tambah', $data);
        // $this->load->view('includes/footer');
    }
    public function tambah_proses()
    {
        $data = [
            'nama_satuan'              => $this->input->post('nama_satuan', true),
        ];

        $config = [
            [
                'field' => 'nama_satuan',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == false) {
            $this->tambah();
        } else {
            $this->satuan_model->insert($data);
            redirect('satuan');
        }
        echo json_encode($data);
        die;
    }

}

/* End of file Satuan.php */
