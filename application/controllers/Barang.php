<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();


        $this->load->model("Barang_model");
        $this->load->model("Kategori_model");
    }

    public function data()
    {
        $data = $this->Barang_model->get_all();

        if ($data) {
            echo json_encode([
                "status"    => 1,
                "message"   => "Data ditemukans",
                "data"      => $data
            ]);
        } else {
            echo json_encode([
                "status"    => 0,
                "message"   => "Data tidak ditemukan",
            ]);
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Barang',
            'data' => $this->Barang_model->get_all() ? $this->Barang_model
                ->order_by('nama', 'asc')
                ->with_kategori('fields:nama')->get_all() : []
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('barang/index', $data);
        $this->load->view('includes/footer');
        // echo json_encode($data);
    }

    public function tambah()
    {
        $data = [
            'title' => 'Barang',
            'kategori' => $this->Kategori_model->get_all()
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('barang/tambah', $data);
        $this->load->view('includes/footer');
    }

    public function tambah_proses()
    {
        // $data = [
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'field_db_tambah'   => $this->input->post('name_field_tambah', true),
        //     'created_at'        => date('Y-m-d H:i:s'),
        // ];

        $data = [
            'nama'              => $this->input->post('nama', true),
            'kode'              => $this->input->post('kode', true),
            'id_kategori'       => $this->input->post('id_kategori', true),
            'stok'              => $this->input->post('stok', true),
            'harga_jual'        => $this->input->post('harga_jual', true),
            'harga_beli'        => $this->input->post('harga_beli', true),
            'id_supplier'       => $this->input->post('id_supplier', true),
        ];

        $config = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'id_kategori',
                'label' => 'Kategori',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'stok',
                'label' => 'Stok',
                'rules' => 'required|trim|numeric'
            ],
            [
                'field' => 'harga_jual',
                'label' => 'Harga jual',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'harga_beli',
                'label' => 'Harga beli',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == false) {
            $this->tambah();
        } else {
            $this->Barang_model->insert($data);
            redirect('barang');
        }
    }

    public function ubah($id)
    {
        $data = [
            'title' => 'Barang',
            'barang' => $this->Barang_model->get($id),
            'kategori' => $this->Kategori_model->get_all()
        ];
        $this->load->view('includes/header', $data);
        $this->load->view('barang/ubah', $data);
        $this->load->view('includes/footer');
    }

    public function ubah_proses()
    {
        $data = [
            'id'    => $this->input->post('id'),
            'ubah'  => [
                'nama'              => $this->input->post('nama', true),
                'kode'              => $this->input->post('kode', true),
                'id_kategori'       => $this->input->post('id_kategori', true),
                'stok'              => $this->input->post('stok', true),
                'harga_jual'        => $this->input->post('harga_jual', true),
                'harga_beli'        => $this->input->post('harga_beli', true),
                'id_supplier'       => $this->input->post('id_supplier', true),
            ],
        ];

        $config = [
            [
                'field' => 'id',
                'label' => 'ID',
                'rules' => 'required|trim|numeric'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'id_kategori',
                'label' => 'Kategori',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'stok',
                'label' => 'Stok',
                'rules' => 'required|trim|numeric'
            ],
            [
                'field' => 'harga_jual',
                'label' => 'Harga jual',
                'rules' => 'required|trim'
            ],
            [
                'field' => 'harga_beli',
                'label' => 'Harga beli',
                'rules' => 'required|trim'
            ],
        ];

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == false) {
            $this->ubah($data['id']);
        } else {
            if ($data['id'] == null) {
                $this->session->set_flashdata(
                    'message',
                    '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p style="font-weight: bold; font-size: 50px; text-align: center; color: white;">Terjadi Kesalahan!</p>
            </div>'
                );
                redirect('barang');
            } else {
                $this->Barang_model->update($data['ubah'], $data['id']);
                redirect('barang');
            }
        }
    }

    public function hapus()
    {
        // $data = [
        //     'id'    => $this->input->post('id', true),
        //     'ubah'  => [
        //         'is_deleted'    => 1
        //     ],
        // ];

        $id = $this->input->post('id', true);

        if ($id == null) {
            $this->session->set_flashdata(
                'message',
                '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p style="font-weight: bold; font-size: 50px; text-align: center; color: white;">Terjadi Kesalahan!</p>
                </div>'
            );
            redirect('barang');
        } else {
            $this->Barang_model->delete($id);
            redirect('barang');
        }
    }
}
