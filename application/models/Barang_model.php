<?php

class Barang_model extends Custom_model
{
    public $table           = 'm_barang';
    public $primary_key     = 'id';
    public $soft_deletes    = TRUE;
    public $timestamps      = TRUE;
    public $return_as       = "array";

    public function __construct()
    {
        $this->has_one['kategori'] = [
            'foreign_model' => 'Kategori_model',
            'foreign_table' => 'ref_kategori',
            'foreign_key' => 'id',
            'local_key' => 'id_kategori'
        ];

        parent::__construct();
    }
}
