<?php

class Supplier_model extends Custom_model
{
    public $table           = 'm_supplier';
    public $primary_key     = 'id';
    public $soft_deletes    = TRUE;
    public $timestamps      = TRUE;
    public $return_as       = "array";

    public function __construct()
    {
        $this->has_one['provinsi'] = [
            'foreign_table' => 'tm_provinsi',
            'foreign_key' => 'id',
            'local_key' => 'prov_id'
        ];

        $this->has_one['kabupaten'] = [
            'foreign_table' => 'tm_kabupaten',
            'foreign_key' => 'id',
            'local_key' => 'kab_id'
        ];

        $this->has_one['kecamatan'] = [
            'foreign_table' => 'tm_kecamatan',
            'foreign_key' => 'id',
            'local_key' => 'kec_id'
        ];

        $this->has_one['kelurahan'] = [
            'foreign_table' => 'tm_kelurahan',
            'foreign_key' => 'id',
            'local_key' => 'kel_id'
        ];

        parent::__construct();
    }
}
