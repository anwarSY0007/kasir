<?php

class Admin_model extends Custom_model
{
    public $table           = 'm_admin';
    public $primary_key     = 'id';
    public $soft_deletes    = TRUE;
    public $timestamps      = TRUE;
    public $return_as       = "array";

    public function __construct()
    {
        $this->has_one['level'] = [
            'foreign_model' => 'Level_model',
            'foreign_table' => 'ref_level',
            'foreign_key' => 'id',
            'local_key' => 'id_level'
        ];

        parent::__construct();
    }
}
